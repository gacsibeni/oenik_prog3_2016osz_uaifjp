﻿//-----------------------------------------------------------------------
// <copyright file="Hos.cs" company="OE-NIK">
// No copyright (public domain)
// </copyright>
//-----------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace HeroGame
{
    /// <summary>
    /// A hős osztályunk kódja.
    /// </summary>
    public class Hos : Elem
    {
        /// <summary>
        /// A kör sugarának konstans értéke. Ez gyakorlatilag a hős fejének nagysága.
        /// </summary>
        public const int R = 15;

        /// <summary>
        /// Initializes a new instance of the <see cref="Hos"/> class.
        /// A hős konstruktora. Ez egy pathfigurek-ből összeállít egy figurát, melyet egyesítünk, kombináljuk egy körrel (a fej), majd azt adjuk vissza alakként.
        /// </summary>
        /// <param name="cx">Baselt x pozíció elemtől.</param>
        /// <param name="cy">Baselt y pozíció elemtől.</param>
        public Hos(int cx, int cy)
            : base(cx, cy)
        {
            PathFigure pf = new PathFigure();
            pf.Segments.Add(new LineSegment(new Point(R, 0), true));
            pf.Segments.Add(new LineSegment(new Point(-R, 0), true));
            pf.Segments.Add(new LineSegment(new Point(0, -R), true));
            pf.Segments.Add(new LineSegment(new Point(R, 0), true));
            PathGeometry pg = new PathGeometry(new PathFigure[] { pf });
            Geometry unio = Geometry.Combine(pg, new EllipseGeometry(new Point(0, -(1.5 * R)), R / 1.5, R / 1.5), GeometryCombineMode.Union, null);       // Line Segmentekből egy figura kialakítása
            this.alak = unio;
        }

        /// <summary>
        /// A felfelé mozgás metódusa.
        /// </summary>
        /// <param name="ch">Megadja, mekkora mértékű a felmozgás.</param>
        public void MozgatFel(int ch)
        {
            this.Cy += -ch;
        }

        /// <summary>
        /// A lefelé mozhás metódusa.
        /// </summary>
        /// <param name="ch">Megadja, mekkora mértékű a lefelé mozgás.</param>
        public void MozgatLe(int ch)
        {
            this.Cy += ch;
        }
    }
}
