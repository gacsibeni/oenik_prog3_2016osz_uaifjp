﻿//-----------------------------------------------------------------------
// <copyright file="Hatarfal.cs" company="OE-NIK">
// No copyright (public domain)
// </copyright>
//-----------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace HeroGame
{
    /// <summary>
    /// A határfalakat befoglaló osztály.
    /// </summary>
    public class Hatarfal : Elem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Hatarfal"/> class.
        /// A határfalak visszadobják a rálépő játékost.
        /// </summary>
        /// <param name="cx">Az x koord. Baselt elemtől.</param>
        /// <param name="cy">Az y koord. Baselt elemtől.</param>
        public Hatarfal(int cx, int cy)
            : base(cx, cy)
        {
            RectangleGeometry rg = new RectangleGeometry();
            rg.Rect = new Rect(cx, cy, 800, 10);
            Geometry geom = rg;
            this.alak = geom;
        }
    }
}
