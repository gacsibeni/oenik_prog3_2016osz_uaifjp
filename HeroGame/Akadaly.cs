﻿//-----------------------------------------------------------------------
// <copyright file="Akadaly.cs" company="OE-NIK">
// No copyright (public domain)
// </copyright>
//-----------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace HeroGame
{   
    /// <summary>
    /// Az akadály osztály kódja.
    /// </summary>
    public class Akadaly : Elem
    {
        /// <summary>
        /// A konstans értékek, a rés nagysága illetve egy akadályfal szélessége.
        /// </summary>
        public const int Res = 125, W = 10;        // Rés nagysága, szélesség

        /// <summary>
        /// Egy random definíciója, melyet arra használunk, hogy random helyezzük el a rés helyét a falon, de azért ésszerű keretek között, hogy teljesíthető legyen a feladat.
        /// </summary>
        public static Random Rnd = new Random();

        /// <summary>
        /// A felületünk szélessége és nagysága, azért, hogy tudjuk, mikor ér ki az akadály a képről.
        /// </summary>
        private int fw, fh;

        /// <summary>
        /// Initializes a new instance of the <see cref="Akadaly"/> class.
        /// Az akadályt létrehozó konstruktor.
        /// </summary>
        /// <param name="cx">Az X koord., hogy hol legyen a fal.</param>
        /// <param name="fw">Felült szélte.</param>
        /// <param name="fh">Felület magassága.</param>
        public Akadaly(int cx, int fw, int fh)
            : base(cx, RandomRes(fh))
        {
            this.fw = fw;
            this.fh = fh;
            this.General();
        }

        /// <summary>
        /// A random rés beállításához használt randomszámoló, kell hozzá a felület magassága, hogy a rés nagyjából a közepefele helyezkedjen majd el a falnak.
        /// </summary>
        /// <param name="fh">Felület magassága.</param>
        /// <returns>Egy integert, amit a gmozgó metódusnál kell.</returns>
        public static int RandomRes(int fh)
        {
            return Rnd.Next(Res, fh - Res);
        }

        /// <summary>
        /// Egy akadály igazából egy Geometry group, mely a két téglalapból és a köztük lévő résből tevődik össze. Ennek alakja jön vissza flattenet path geometryként.
        /// </summary>
        public void General()
        {
            GeometryGroup gg = new GeometryGroup();
            gg.Children.Add(new RectangleGeometry(new Rect(-W, -Cy + 30, W, Cy - Res / 2)));
            gg.Children.Add(new RectangleGeometry(new Rect(-W, Res / 2, W, this.fh - Cy - Res / 2 - 30)));
            this.alak = gg.GetFlattenedPathGeometry();
        }
        
        /// <summary>
        /// A minden tick-ben futó mozgató metódus, az akadályok balra tartanak, és vizsgáljuk, hogy ha kilépnek a képernyő bal oldalán, akkor már generálódik az új, egy random réssel, ami máshol lesz mint az előbb.
        /// </summary>
        public void Mozog()
        {
            this.Cx = this.Cx - 2;
            if (Alak.Bounds.Right <= 0)
            {
                this.Cx = this.fw + W;
                this.Cy = RandomRes(this.fh);
                this.General();
            }
        }
    }
}
