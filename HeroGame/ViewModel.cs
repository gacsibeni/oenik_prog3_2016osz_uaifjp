﻿//-----------------------------------------------------------------------
// <copyright file="ViewModel.cs" company="OE-NIK">
// No copyright (public domain)
// </copyright>
//-----------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeroGame
{
    /// <summary>
    /// A viewmodel osztályunk.
    /// </summary>
    public class ViewModel
    {
        /// <summary>
        /// Gets or sets a Hos object.
        /// A játékos által irányított hős osztály, tulajdonságként a ViewModelben használunk egy példányt.
        /// </summary>
        /// <value>Hos példány szükséges.</value>
        public Hos Hos { get; set; }

        /// <summary>
        /// Gets or sets a Hatarfal object.
        /// A képernyőn két falunk van, melyek speciálisak, hiszen a hős illetve a főellenség érintéskor visszalökődnek róla, nem mehetnek tovább rajta.
        /// </summary>
        /// <value>Hatarfal példény szükséges.</value>
        public Hatarfal FelsoHatarfal { get; set; }

        /// <summary>
        /// Gets or sets a Hatarfal object.
        /// Az alsó megfelelője a fent említett felső határfalnak. A szabályai ugyanazok, helyzete más a képernyőn.
        /// </summary>
        /// <value>Hatarfal példány szükséges.</value>
        public Hatarfal AlsoHatarfal { get; set; }

        /// <summary>
        /// Gets or sets Akadalyok object.
        /// Egy listában tároljuk a kérpenyőn megjelenő és folyton mozgó akadályokat.
        /// </summary>
        /// <value>Egy Akadaly típusú Lista szükséges.</value>
        public List<Akadaly> Akadalyok { get; set; }

        /// <summary>
        /// Gets or sets a Foellenseg object.
        /// A képernyőn megjelenő főellenség tulajdonsága.
        /// </summary>
        /// <value>Foellenseg példány szükséges.</value>
        public Foellenseg Foellenseg { get; set; }

        /// <summary>
        /// Gets or sets a Lovedek object.
        /// A játékos lövedékét és az ellenfél lövedékét külön kezeljük, mert ezek egyszerre is a képernyőn lehetnek, illetve más mozgási szabályok vonatkoznak rájuk, másmerre haladnak stb.
        /// </summary>
        /// <value>Lovedek példány szükséges</value>
        public Lovedek JatekosLovedeke { get; set; }

        /// <summary>
        /// Gets or sets a Lovedek object.
        /// Az ellenfél lövedéke, külön kezelt a játékosétól, ld. játékos lövedéke.
        /// </summary>
        /// <value>Lovedek példány szükséges.
        /// </value>
        public Lovedek EllenfelLovedeke { get; set; }

        /// <summary>
        /// Gets or sets an integer.
        /// Az ellenfél életpontjait tartjuk nyílván tetszőleges integer formában. Ha elfogy, a játékos nyert.
        /// </summary>
        /// <value>Integer, játék nehézségének állítására kiváló érték lehet.</value>
        public int EllenfelEletpont { get; set; }

        /// <summary>
        /// Gets or sets an integer.
        /// A játékos életének száma, tetszőleges integer, de 10 szorzatai ajánlottak, mert tizesével fogyhat.
        /// </summary>
        /// <value>Integer, 10 szorzatai ajánlottak.</value>
        public int Eletpont { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the enemy has fired a shot or not.
        /// Ez a tulajdonság felelős azért, hogy egyszerre csak egy lövedék elől kelljen a játékosnak kitérnie, annál többet ne lőhessen ki az ellenség.
        /// </summary>
        /// <value>Bool érték.</value>
        public bool EllenfelLott { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the player has fired or not.
        /// Ez a tulajdonság felelős azért, hogy egyszerre csak egy lövedék jöhessen a játékostól, ne lőhessen egyfolytában.
        /// </summary>
        /// <value>Bool érték.</value>
        public bool JatekosLott { get; set; }

        /// <summary>
        /// Gets a value indicating whether the game is over or not.
        /// A játék végét figyelő tulajdonság. Mivel kétféleképpen lehet vége, játékos vagy ellenség hal meg, ezért ezt a kettőt vagyolja.
        /// </summary>
        /// <value>Boolal tér vissza, be nem állítható!</value>
        public bool Vege
        {
            get { return this.Eletpont <= 0 || this.EllenfelEletpont <= 0; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the hero has reached the second phase of game.
        /// Azt vizsgálja, hogy elértük-e a játék második részét.
        /// </summary>
        /// <value>Bool érték.</value>
        public bool MasodikFazis { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the boss appeared or not.
        /// Azt vizsgálja, hogy a főellenség megjelent-e.
        /// </summary>
        /// <value>Bool érték.</value>
        public bool FoellensegMegjelent { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the hero has touched any other element or not.
        /// Azt vizsgálja, volt-e az adott tick-ben ütközése a játékosnak valamilyen elemmel. 
        /// </summary>
        /// <value>Bool érték.</value>
        public bool Utkozott { get; set; }
    }
}
