﻿//-----------------------------------------------------------------------
// <copyright file="Elem.cs" company="OE-NIK">
// No copyright (public domain)
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace HeroGame
{
    /// <summary>
    /// Az elem absztakt osztály kódja. Minden későbbi megjelenő alak ebből származik.
    /// </summary>
    public abstract class Elem
    {
        /// <summary>
        /// Az a Geometry visszatérési értékű alak tulajdonság, amellyel később dolgozunk.
        /// </summary>
        protected Geometry alak;

        /// <summary>
        /// Ez a két paraméter határozza meg a képernyőn elfoglalt helyét az objektumnak.
        /// </summary>
        private int cx, cy;

        /// <summary>
        /// Initializes a new instance of the <see cref="Elem"/> class.
        /// Az elem osztály konstruktora, melyre baselni fog a többi leszármazott osztály konstruktora.
        /// </summary>
        /// <param name="cx">A cx paraméter.</param>
        /// <param name="cy">A cy paraméter.</param>
        public Elem(int cx, int cy)
        {
            this.cx = cx;
            this.cy = cy;
        }

        /// <summary>
        /// Gets or sets the cx value.
        /// </summary>
        /// <value>
        /// Tetszőleges integer.
        /// </value>
        public int Cx
        {
            get { return this.cx; }
            set { this.cx = value; }
        }

        /// <summary>
        /// Gets or sets the cy value.
        /// </summary>
        /// <value>Tetszőleges integer</value>
        public int Cy
        {
            get { return this.cy; }
            set { this.cy = value; }
        }

        /// <summary>
        /// Gets the Geometry alak value.
        /// A Geometry visszatérési értékű mező tulajdonságként. Set nincs, gettel áll be mindig.
        /// </summary>
        /// <value>
        /// A cx, cy tulajdonságokból generálódik híváskor.
        /// </value>
        public Geometry Alak
        {
            get
            {
                Geometry copy = this.alak.Clone();
                TransformGroup tg = new TransformGroup();
                tg.Children.Add(new TranslateTransform(this.cx, this.cy));
                copy.Transform = tg;
                return copy.GetFlattenedPathGeometry();
            }
        }

        /// <summary>
        /// Az ütközés metódusa, mely meghatározza, hogy két, akár különböző leszármazott osztály példánya ütközik-e vagy sem egymással.
        /// </summary>
        /// <param name="elem">Az elem paraméter a másik elem, amivel az összeérést vizsgáljuk. </param>
        /// <returns>Bool értéket, amely az ütközés</returns>
        public bool Utkozik(Elem elem)
        {
            return Geometry.Combine(this.Alak, elem.Alak, GeometryCombineMode.Intersect, null).GetArea() > 0;
        }
    }
}
