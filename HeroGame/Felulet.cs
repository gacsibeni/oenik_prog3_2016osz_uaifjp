﻿//-----------------------------------------------------------------------
// <copyright file="Felulet.cs" company="OE-NIK">
// No copyright (public domain)
// </copyright>
//-----------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;

namespace HeroGame
{
    /// <summary>
    /// A felületünk osztálya.
    /// </summary>
    public class Felulet : FrameworkElement
    {
        /// <summary>
        /// A viewmodelünk példánya kell ide.
        /// </summary>
        public ViewModel Vm;

        /// <summary>
        /// A dispatcher timer példánya.
        /// </summary>
        public DispatcherTimer Timer;

        /// <summary>
        /// A stopperóra példánya.
        /// </summary>
        public Stopwatch Stopwatch;

        /// <summary>
        /// Egy random, melyet felhasználunk futáskor.
        /// </summary>
        public Random R = new Random();

        /// <summary>
        /// A timespan, ami az első fázis vége tulajdonképpen.
        /// </summary>
        public TimeSpan Ts = new TimeSpan(0, 0, 30);
       
        /// <summary>
        /// Initializes a new instance of the <see cref="Felulet"/> class.
        /// A felület konstruktora. Lekreál minden definiált tulajdonságot, a határfalakat, inicializál egy listát melby majd belegenerálódik az akadály futáskor.
        /// </summary>
        public Felulet()
        {
            this.Vm = new ViewModel() { Hos = new Hos(15, 300), FelsoHatarfal = new Hatarfal(0, 10), AlsoHatarfal = new Hatarfal(0, 165), Akadalyok = new List<Akadaly>() };
            this.Loaded += this.Felulet_Loaded;
            this.Timer = new DispatcherTimer();
            this.Stopwatch = new Stopwatch();
            this.Timer.Interval = new TimeSpan(0, 0, 0, 0, 20);
            this.Timer.Tick += this.Timer_Tick;
            this.Timer.Start();
            this.Stopwatch.Start();
            this.Vm.Eletpont = 100;
            this.Vm.EllenfelEletpont = 3;
        }

        /// <summary>
        /// A timer minden egyes tickjekor lefutó metódus. Gyakorlatilag ez vezérli a játékot. Boolokkal több ágra van bontva, melyek jelzik a játék adott fázisát, ezáltal azt, hogy mi menjen a képernyőn.
        /// </summary>
        /// <param name="sender">A küldő objektum.</param>
        /// <param name="e">Event argumentumok.</param>
        public void Timer_Tick(object sender, EventArgs e)
        {            
            if (this.Stopwatch.Elapsed < this.Ts)
            {
                bool marutkozott = false;
                foreach (Akadaly ak in this.Vm.Akadalyok)
                {
                    ak.Mozog();
                    if (ak.Utkozik(this.Vm.Hos))
                    {
                        if (!this.Vm.Utkozott)
                        {
                            this.Vm.Eletpont = this.Vm.Eletpont - 10;
                        }

                        marutkozott = true;
                    }
                }

                this.Vm.Utkozott = marutkozott;

                if (this.Vm.Vege)
                {
                    this.Timer.Stop();
                    this.Stopwatch.Stop();
                }
            }
            else
            {
                this.Vm.Akadalyok.Clear();
                this.Vm.MasodikFazis = true;
            }

            if (this.Vm.MasodikFazis)
            {
                if (this.Vm.Vege)
                {
                    this.Timer.Stop();
                    this.Stopwatch.Stop();
                }

                if (!this.Vm.FoellensegMegjelent)
                {
                    this.Vm.Foellenseg = new Foellenseg(550, 200);
                    this.Vm.FoellensegMegjelent = true;
                }
                else 
                {
                    if (!this.Vm.EllenfelLott)
                    {
                        this.Vm.EllenfelLott = true;
                        this.Vm.EllenfelLovedeke = new Lovedek(this.Vm.Foellenseg.Cx - 300, (this.Vm.Foellenseg.Cy / 2) - 7, 13);
                    }

                    int rx = this.R.Next(1, 10);
                    if (rx > 4)
                    {
                        this.Vm.Foellenseg.Mozog(this.Vm.AlsoHatarfal, this.Vm.FelsoHatarfal);

                        if (this.Vm.EllenfelLott)
                        {
                            if (this.Vm.EllenfelLovedeke.Utkozik(this.Vm.Hos))
                            {
                                this.Vm.Eletpont += -30;
                                this.Vm.EllenfelLott = false;
                            }
                            else
                            {
                                if (this.Vm.EllenfelLovedeke.Cx < -550)
                                {
                                    this.Vm.EllenfelLott = false;
                                }
                            }

                            this.Vm.EllenfelLovedeke.Repul(false);
                        }
                    }    
                }

                if (this.Vm.JatekosLott == true)
                {
                    if (this.Vm.JatekosLovedeke.Utkozik(this.Vm.Foellenseg))
                    {
                        this.Vm.EllenfelEletpont += -1;
                        this.Vm.JatekosLott = false;
                    }
                    else
                    {
                        if (this.Vm.JatekosLovedeke.Cx > (int)ActualWidth)
                        {
                            this.Vm.JatekosLott = false;
                        }
                    }

                    this.Vm.JatekosLovedeke.Repul(true);
                }
            }

            this.InvalidateVisual();
        }

        /// <summary>
        /// A felületünk beültődésekor lefutni szükséges elemek. Ilyenkor kreálódnak az akadályok, melyek bekerülnek a listába, illetve a felület keydown eventjét megkapja a window.
        /// </summary>
        /// <param name="sender">A küldő objektum</param>
        /// <param name="e">Esemény argumentumok.</param>
        public void Felulet_Loaded(object sender, RoutedEventArgs e)
        {
            (this.Parent as Window).KeyDown += this.Felulet_KeyDown;
            {
                int tav = this.R.Next(60, 65);
                {
                    for (int i = 0; i <= 6; i++)
                    {
                        {
                            this.Vm.Akadalyok.Add(new Akadaly(i * tav + (2 * i - 1) * Akadaly.W, (int)ActualWidth, (int)ActualHeight));
                        }
                    }
                }
            }
        }

        /// <summary>
        /// A kirenderelni kívánt elemek. Boolokkal elválasztjuk a különböző fázisokat, ezekben eltérőek e a megjelenített elemek. Hogy mi hogyan nézzen ki, gyakorlatilag itt állítódik be.
        /// </summary>
        /// <param name="kirajzolando">Az összes kirajzolandó objektum.</param>
        protected override void OnRender(System.Windows.Media.DrawingContext kirajzolando)
        {
            kirajzolando.DrawGeometry(Brushes.Orange, new Pen(Brushes.Black, 2), this.Vm.Hos.Alak);

            kirajzolando.DrawGeometry(Brushes.Green, new Pen(Brushes.Black, 1), this.Vm.FelsoHatarfal.Alak);

            kirajzolando.DrawGeometry(Brushes.Green, new Pen(Brushes.Black, 1), this.Vm.AlsoHatarfal.Alak);

            foreach (Akadaly ak in this.Vm.Akadalyok)
            {
                kirajzolando.DrawGeometry(Brushes.Green, new Pen(Brushes.Green, 2), ak.Alak);
            }

            FormattedText ft = new FormattedText("Health: " + this.Vm.Eletpont.ToString(), System.Globalization.CultureInfo.CurrentCulture, System.Windows.FlowDirection.LeftToRight, new Typeface(new FontFamily("Arial"), FontStyles.Normal, FontWeights.ExtraBold, FontStretches.Normal), 18, Brushes.Black);
            kirajzolando.DrawGeometry(Brushes.Orange, new Pen(), ft.BuildGeometry(new Point(5, 0)));

            FormattedText ft2 = new FormattedText("Time: " + this.Stopwatch.Elapsed.ToString("ss\\:ff"), System.Globalization.CultureInfo.CurrentCulture, System.Windows.FlowDirection.LeftToRight, new Typeface(new FontFamily("Arial"), FontStyles.Normal, FontWeights.ExtraBold, FontStretches.Normal), 18, Brushes.Black);
            kirajzolando.DrawGeometry(Brushes.Orange, new Pen(), ft2.BuildGeometry(new Point(465, 0)));

            if (this.Vm.MasodikFazis)
            {
                FormattedText ft3 = new FormattedText("Fighting Phase!", System.Globalization.CultureInfo.CurrentCulture, System.Windows.FlowDirection.LeftToRight, new Typeface(new FontFamily("Arial"), FontStyles.Normal, FontWeights.ExtraBold, FontStretches.Normal), 18, Brushes.Black);
                kirajzolando.DrawGeometry(Brushes.Orange, new Pen(), ft3.BuildGeometry(new Point(220, 0)));

                FormattedText ft4 = new FormattedText("Boss Health: " + this.Vm.EllenfelEletpont.ToString(), System.Globalization.CultureInfo.CurrentCulture, System.Windows.FlowDirection.LeftToRight, new Typeface(new FontFamily("Arial"), FontStyles.Normal, FontWeights.ExtraBold, FontStretches.Normal), 18, Brushes.Black);
                kirajzolando.DrawGeometry(Brushes.Orange, new Pen(), ft4.BuildGeometry(new Point(440, 340)));

                kirajzolando.DrawGeometry(Brushes.Pink, new Pen(Brushes.Black, 2), this.Vm.Foellenseg.Alak);
            }

            if (this.Vm.JatekosLott)
            {
                kirajzolando.DrawGeometry(Brushes.Red, new Pen(Brushes.Red, 1), this.Vm.JatekosLovedeke.Alak);
            }

            if (this.Vm.EllenfelLott)
            {
                kirajzolando.DrawGeometry(Brushes.Green, new Pen(Brushes.Green, 1), this.Vm.EllenfelLovedeke.Alak);
            }

            if (this.Vm.Vege)
            {
                FormattedText ft5 = new FormattedText("Játék vége!", System.Globalization.CultureInfo.CurrentCulture, System.Windows.FlowDirection.LeftToRight, new Typeface(new FontFamily("Arial"), FontStyles.Normal, FontWeights.ExtraBold, FontStretches.Normal), 70, Brushes.Black);
                kirajzolando.DrawGeometry(Brushes.Orange, new Pen(), ft5.BuildGeometry(new Point(90, 100)));
            }
        }

        /// <summary>
        /// A billentyűnyomást érzékeljük. A kódban a space, a fel és a le van kezelve.
        /// </summary>
        /// <param name="sender">A küldő objektum.</param>
        /// <param name="e">Event argumentumok.</param>
        private void Felulet_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (this.Vm.Vege)
            {
                return;
            }

            if (e.Key == Key.Up)
            {             
                this.Vm.Hos.MozgatFel(20);
                if (this.Vm.Hos.Utkozik(this.Vm.FelsoHatarfal))
                {
                    this.Vm.Hos.MozgatLe(100);
                    this.Vm.Eletpont += -10;
                }
            }

            if (e.Key == Key.Down)
            {
                this.Vm.Hos.MozgatLe(20);
                if (this.Vm.Hos.Utkozik(this.Vm.AlsoHatarfal))
                {
                    this.Vm.Hos.MozgatFel(100);
                    this.Vm.Eletpont += -10;
                }
            }

            if (e.Key == Key.Space && this.Vm.FoellensegMegjelent && !this.Vm.JatekosLott)
            {
                this.Vm.JatekosLott = true;
                this.Vm.JatekosLovedeke = new Lovedek(this.Vm.Hos.Cx, (this.Vm.Hos.Cy / 2) - 7, 10);     
            }
        }
   }
}
