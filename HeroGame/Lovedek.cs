﻿//-----------------------------------------------------------------------
// <copyright file="Lovedek.cs" company="OE-NIK">
// No copyright (public domain)
// </copyright>
//-----------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace HeroGame
{
    /// <summary>
    /// A lövedék elemünk kódja.
    /// </summary>
    public class Lovedek : Elem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Lovedek"/> class.
        /// A lövedéket konstruktora. X és Y baselt elemtől.
        /// </summary>
        /// <param name="cx">A  cx paraméter, örökölt az elemtől.</param>
        /// <param name="cy">A cy paraméter, örökölt az elemtől.</param>
        /// <param name="r">Az r paraméter, a kör sugara, tehát magadja milyen nagy legyen a golyó.</param>
        public Lovedek(int cx, int cy, int r) : base(cx, cy)
        {
            EllipseGeometry eg = new EllipseGeometry(new Point(cx, cy), r, r);
            Geometry geom = eg;
            this.alak = geom;
        }

        /// <summary>
        /// Ez a  metódus egyszerűen változtatja az örökölt cx paramétert, azaz a golyó horizontális helyzetét.
        /// </summary>
        /// <param name="jatekose">A bool, amely meghatározza, merrefelé halad a golyó, tehát, hogy ki lőtte ki.</param>
        public void Repul(bool jatekose)
        {
            if (jatekose)
            {
                this.Cx += 12;
            }
            else
            {
                this.Cx += -20;
            }
        }
    }
}
