﻿//-----------------------------------------------------------------------
// <copyright file="Foellenseg.cs" company="OE-NIK">
// No copyright (public domain)
// </copyright>
//-----------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace HeroGame
{
    /// <summary>
    /// A főellenség osztály kódja.
    /// </summary>
    public class Foellenseg : Elem
    {
        /// <summary>
        /// Egy konstants, gyakrolatilag az ellenség fejének nagysága.
        /// </summary>
        public const int R = 40;

        /// <summary>
        /// Egy random, mely a mozgásnál használatos.
        /// </summary>
        public static Random Rnd = new Random();

        /// <summary>
        /// Initializes a new instance of the <see cref="Foellenseg"/> class.
        /// Hasonló a hőshöz, csupán másmilyen alakot paraméterezünk össze pathfigureként, linesegmentekből.
        /// </summary>
        /// <param name="cx">Az x helye.</param>
        /// <param name="cy">Az y helye.</param>
        public Foellenseg(int cx, int cy)
            : base(cx, cy)
        {
            PathFigure pf = new PathFigure();

            pf.Segments.Add(new LineSegment(new Point(R, 0), true));
            pf.Segments.Add(new LineSegment(new Point(-R, 0), true));
            pf.Segments.Add(new LineSegment(new Point(-R, -R), true));
            pf.Segments.Add(new LineSegment(new Point(R, -R), true));
            pf.Segments.Add(new LineSegment(new Point(R, 0), true));
            PathGeometry pg = new PathGeometry(new PathFigure[] { pf });
            Geometry unio = Geometry.Combine(pg, new EllipseGeometry(new Point(0, -(1.5 * R)), R / 2, R / 2), GeometryCombineMode.Union, null);       // Line Segmentekből egy figura kialakítása
            this.alak = unio;
        }

        /// <summary>
        /// A hős random mozgásának metódusa. Figyeljük benne, hogy ne csússzunk ki a képről, ezért kell neki a két határfal példány.
        /// </summary>
        /// <param name="also">Az alsó határfal példánya kell.</param>
        /// <param name="felso">A felső határfal példánya kell.</param>
        public void Mozog(Hatarfal also, Hatarfal felso)
        {
            int vx = Rnd.Next(1, 10);

            if (vx <= 5)
            {
                if (!this.Utkozik(also))
                {
                    this.Cy += 5;
                }
            }
            else
            {
                if (!this.Utkozik(felso))
                {
                    this.Cy += -5;
                }
            }
        }
    }
}
